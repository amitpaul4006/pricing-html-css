const toggle = () => {
  fetch("./data.json")
    .then((response) => {
      return response.json();
    })
    .then((data) => {
      const elem1 = document.getElementById('low');
      const elem2 = document.getElementById('medium');
      const elem3 = document.getElementById('high');

      const check = document.querySelector("#checkbox");
      if (!check.checked) {
        elem1.innerHTML = `<label>$</label>${data.monthly.basic}`;
        elem2.innerHTML = `<label>$</label>${data.monthly.professional}`;
        elem3.innerHTML = `<label>$</label>${data.monthly.master}`;
      } else {
        elem1.innerHTML = `<label>$</label>${data.annually.basic}`;
        elem2.innerHTML = `<label>$</label>${data.annually.professional}`;
        elem3.innerHTML = `<label>$</label>${data.annually.master}`;
      }
    })
    .catch((error) => {
        console.error(error);
    })

  // const elem1 = document.getElementById('low');
  // const elem2 = document.getElementById('medium');
  // const elem3 = document.getElementById('high');

  // if(elem1.innerHTML == "<label>$</label>19.99")
  // {
  //     elem1.innerHTML = "<label>$</label>199.99";
  // }
  // else{
  //     elem1.innerHTML = "<label>$</label>19.99";
  // }

  // if(elem2.innerHTML == "<label>$</label>24.99"){

  //     elem2.innerHTML = "<label>$</label>249.99";
  // }
  // else{
  //     elem2.innerHTML = "<label>$</label>24.99";
  // }

  // if(elem3.innerHTML == "<label>$</label>39.99"){
  //     elem3.innerHTML = "<label>$</label>399.99";
  // }
  // else{
  //     elem3.innerHTML = "<label>$</label>39.99";
  // }
};
